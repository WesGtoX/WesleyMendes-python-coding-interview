from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from app.store.models import Products


class ProductTestCase(TestCase):

    def setup(self):
        ...

    def test_create(self):
        data = dict(name='test', is_active=True)

        response = self.client.post(reverse('store:products-list'), data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(data, response.data)

    def test_activate(self):
        p = Products.objects.create(name='test', is_active=False)
        response = self.client.post(reverse('store:products-activate'), args={'pk': p.pk}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        product = Products.objects.get(pk=1)
        self.assertTrue(product.is_active)

        response = self.client.post(reverse('store:products-activate'), args={'pk': p.pk}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(product.is_active)
