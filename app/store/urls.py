from django.urls import include, path
from rest_framework import routers

from app.store import views

app_name = 'store'

router = routers.DefaultRouter()
router.register(r'products', views.ProductsViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
