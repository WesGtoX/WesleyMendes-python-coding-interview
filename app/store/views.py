from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from app.store.models import Products
from app.store.serializers import ProductSerializer


class ProductsViewSet(viewsets.ModelViewSet):
     queryset = Products.objects.all()
     serializer_class = ProductSerializer
     permission_classes = []

     @action(methods=['POST'], detail=True)
     def activate(self, request, pk):
          product = self.queryset.get(pk=pk)
          product.is_active = product.is_active == True
          product.save()
          return Response({}, status=status.HTTP_200_OK)
